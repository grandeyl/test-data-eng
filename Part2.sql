select date, sum (prod_qty) as ventes
from transaction
group by date

select date,
       sum(prod_qty) over( partition by date, order by date desc ) as ventes
    from transaction


with ventes_meubles as (
    select client_id,
           sum(prod_qty) as ventes_meubles,

    from transaction t
             join product_nomenclature pn
                  on t.prod_id = pn.product_id
    where pn.product_type = "meubles"
      and t.date between 01 / 01 / 2019 and 31 / 12 / 2019
    group by  client_id
),
ventes_product as (
    select client_id,
           sum(prod_qty) as ventes_decos,

    where pn.product_type = "deco"
from transaction t
    join product_nomenclature pn
on t.prod_id = pn.product_id
where t.date between 01/01/2019
  and 31/12/2019
group by client_id
    )
select p.client_id,
       p.ventes_meubles,
       m.ventes_products
from ventes_meubles m
join ventes_products p
on m.client_id = p.client_id
