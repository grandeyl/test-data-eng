import re

import numpy as np
import pandas as pd
from pandasql import sqldf


# This is a sample Python script.

# Press ⇧F10 to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.


def import_files():
    drugs = pd.read_csv("Python_test_DE/drugs.csv", sep=",")
    clinical_trials = pd.read_csv("Python_test_DE/clinical_trials.csv", sep=",")
    pubmed = pd.read_csv("Python_test_DE/pubmed.csv", sep=",")
    return drugs, clinical_trials, pubmed


def add_foreign_key_to_pubmed(pubmed):
    df_pubmed = pubmed['title'].astype(str).str.extract(
        pat='(DIPHENHYDRAMIN)|(TETRACYCLINE)|(ETHANOL)|(ATROPINE)|(EPINEPHRINE)|(ISOPRENALINE)|(BETAMETHASONE)',
        flags=re.IGNORECASE)
    pubmed["drug"] = merge_dataframes(df_pubmed)
    pubmed["drug"] = pubmed["drug"].str.upper()


def add_foreign_key_to_clinical_trials(clinical_trials):
    df_clinical_trials = clinical_trials['scientific_title'].astype(str).str.extract(
        pat='(DIPHENHYDRAMIN)|(TETRACYCLINE)|(ETHANOL)|(ATROPINE)|(EPINEPHRINE)|(ISOPRENALINE)|(BETAMETHASONE)',
        flags=re.IGNORECASE)
    clinical_trials["drug"] = merge_dataframes(df_clinical_trials)
    clinical_trials["drug"] = clinical_trials["drug"].str.upper()


def merge_dataframes(dataframe):
    return dataframe[0].fillna("") + dataframe[1].fillna("") + dataframe[
        2].fillna("") + dataframe[3].fillna("") + dataframe[4].fillna("") + dataframe[
               5].fillna("") + dataframe[6].fillna("")


def join_clinical_trials_and_pubmed_using_drugs(dataframe_1, dataframe_2):
    a_outer = pd.merge(dataframe_1, dataframe_2, on="drug", how="outer", suffixes=("_clinical", "_publication"))
    a_outer = a_outer.replace(r'^\s*$', np.nan, regex=True)
    a_outer = a_outer[~a_outer['drug'].isnull()]
    return a_outer


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    drugs, clinical_trials, pubmed = import_files()

    add_foreign_key_to_pubmed(pubmed)
    add_foreign_key_to_clinical_trials(clinical_trials)
    a_outer = join_clinical_trials_and_pubmed_using_drugs(clinical_trials, pubmed)

    a_outer.to_json("my_output.json")
